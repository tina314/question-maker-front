import {Component, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Question} from '../question.model';
import {Observable, merge} from 'rxjs';
import {QuestionService} from '../question.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator, PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.css']
})
export class QuestionListComponent implements OnInit,  OnChanges {

  // tslint:disable-next-line:ban-types
  questions: any;  // bilo je Question[]
  length: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private questionService: QuestionService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
   // this.questionService.fetchQuestions().subscribe((questions: Question[]) => {
   //            this.questions = questions;
   // });
    this.questionService.fetchQuestions(0, 100).subscribe(
      (response) => {
        // @ts-ignore
        this.questions = response.body;
      }
    );
  }

  ngAfterViewInit(): void {
    this.fetch(true);
  }

  fetch(setPaginator: boolean) {
    console.log('Da li si ti undefined ' + this.paginator);
    this.questionService.fetchQuestions(this.paginator.pageIndex || 0, this.paginator.pageSize || 10).subscribe(
      (response) => {
        this.questions = response.body;
        this.length = +response.headers.get('X-total-elements');
        // this.dataSource = new MatTableDataSource<Question>(this.questions);  I BEZ OVOGGG
        if (setPaginator) {
          // this.dataSource.paginator = this.paginator;
        }
      }
    );
  }

  sendRequest(event: PageEvent) {
    this.questionService.fetchQuestions(event.pageIndex || 0, event.pageSize || 10).subscribe(
      (response) => {
        this.questions = response.body;
        this.length = +response.headers.get('X-total-elements');
      }
    );
  }

  onNewQuestion() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  goToEdit(id: number) {
    this.router.navigate([id + '/edit'], {relativeTo: this.route});
  }

  // ngOnDestroy() {
  //   this.subscription.unsubscribe();
  // }
  deleteQuestion(id: number) {
    this.questionService.deleteQuestion(id).subscribe(
      deletedQuestion => {
        alert('You successfully deleted the question!');
        // this.questionService.fetchQuestions().subscribe(
        //   (questions: Question[]) => this.questions = questions
        // );
        this.questionService.fetchQuestions(0, 100).subscribe(
          (response) => {
            // @ts-ignore
            this.questions = response.body;
          }
        );
      }
    );
  //   this.questionService.fetchQuestions().subscribe((questions: Question[]) => {
  //     this.questions = questions;
  // });
    this.questionService.fetchQuestions(0, 100).subscribe(
      (response) => {
        // @ts-ignore
        this.questions = response.body;
      }
    );
  }

  // DA LI MORA OVO
  ngOnChanges(changes: SimpleChanges): void {
    // this.questionService.fetchQuestions().subscribe((questions: Question[]) => {
    //   this.questions = questions;
    // });
    this.questionService.fetchQuestions(0, 100).subscribe(
      (response) => {
        // @ts-ignore
        this.questions = response.body;
      }
    );
  }
}
