import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {FormArray} from "@angular/forms";

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {
  @Input() name;
  checked = false;
  indeterminate = false;
  labelPosition: 'before' | 'after' = 'after';
  disabled = false;


  constructor(private elRef: ElementRef) { }

  ngOnInit(): void {
  }

  // getAnswerControls() {
  //   console.log('Da li je ovo forma');
  //   console.log(this.elRef.nativeElement.parentElement('f'));
  //   return (this.elRef.nativeElement.parentElement('f').get('answers') as FormArray).controls;
  //   // return (this.questionForm.get('answers') as FormArray).controls;
  // }
}
