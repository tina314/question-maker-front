import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Answer} from './answer.model';

// @ts-ignore
@Injectable({providedIn: 'root'})
export class AnswersService {

  private answers: Answer[] = [];

  constructor(private http: HttpClient) {
  }


  // fetchQuestions() {
  //   return this.http.get('http://localhost:8080/api/questions');
  // }

  // tslint:disable-next-line:typedef
  // getQuestion(index: number) {
  //   // return this.questions[index];
  //   return this.http.get('http://localhost:8080/api/questions/' + index);
  // }

  // tslint:disable-next-line:typedef
  // addQuestion(question: Question) {
  //   return this.http.post('http://localhost:8080/api/questions', question);
  // }

  addAnswer(answer: Answer) {
    return this.http.post('http://localhost:8080/api/questions/' + answer.questionId + '/answers', answer);
  }


  // updateQuestion(id: number, question: Question) {
  //   console.log('Ovo je question!');
  //   console.log(question);
  //   return this.http.put('http://localhost:8080/api/questions/' + id, question);
  // }

  deleteAnswer(questionId: number, id: number) {
    return this.http.delete('http://localhost:8080/api/questions/' + questionId + '/answers/' + id);
  }
}
