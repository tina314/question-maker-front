import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {QuestionService} from '../question.service';
import {Question} from '../question.model';
import {Answer} from '../answer/answer.model';
import {AnswerAddDialog} from './answer-add-dialog';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {AnswersService} from "../answer/answers.service";

@Component({
  selector: 'app-question-edit',
  templateUrl: './question-edit.component.html',
  styleUrls: ['./question-edit.component.css']
})
export class QuestionEditComponent implements OnInit {

  id: number;
  questionForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private questionService: QuestionService,
    private router: Router,
    public dialog: MatDialog,
    private answersService: AnswersService
  ) {

    this.questionForm = new FormGroup({
      id: new FormControl(''),
      questionText: new FormControl('', Validators.required),
      points: new FormControl(0, Validators.required),
      questionType: new FormControl('', Validators.required),
      answers: new FormArray([])
    });

  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.id = +params.id;
      this.initForm();
      // this.initForm();
    });
  }

  // tslint:disable-next-line:typedef
  private initForm() {
    // let questionText = '';
    // let points = 0;
    // let questionType = 'Radio Button';
    // let answers = new FormArray([]);


    this.questionService.getQuestion(this.id).subscribe(
      (question: any) => {


        // this.questionForm.setValue({
        //   questionText: new FormControl( question.question.valueOf(), Validators.required),
        //   points: new FormControl(question.points, Validators.required),
        //   questionType: new FormControl(question.questionType, Validators.required),
        //  answers: new FormArray(answersArray)
        //   }
        // );

        this.questionForm.controls.questionText.setValue(question.questionText);
        this.questionForm.controls.points.setValue(question.points);
        this.questionForm.controls.questionType.setValue(question.questionType);
        this.questionForm.controls.id.setValue(question.id);

        for (const a of question.answers) {
          console.log(a);
          (this.questionForm.get('answers') as FormArray)
            .push(new FormGroup(
              {
                answer: new FormControl(a.answer),
                correct: new FormControl(a.correct),
                questionId: new FormControl(question.id),
                id: new FormControl(a.id)}));
        }
        console.log(this.questionForm.getRawValue());
        // const answersArray = this.questionForm.get('answers') as FormArray;
        //
        // for (const answer of question.answers) {
        //   answersArray.push(new FormControl(answer));
        // }
      }
    );


    // this.questionForm = new FormGroup({
    //   questionText: new FormControl(questionText, Validators.required),
    //   points: new FormControl(points, Validators.required),
    //   questionType: new FormControl(questionType, Validators.required),
    //   answers: answers
    // });
  }

  onSubmit() {
    this.questionService.updateQuestion(this.id, this.questionForm.value).subscribe(
      (question) => this.onCancel(),
      error => {
        console.log('Desila se greskaaaaaa');
      }
    );
    console.log('Ovo je questionForm.value');
    console.log(this.questionForm.value);
    // this.onCancel();
  }

  onCancel() {
    this.router.navigate(['../../'], {relativeTo: this.route});
  }

  // onAddAnswer() {
  //   (this.questionForm.get('answers') as FormArray).push(new FormControl(null));
  // }

  getAnswerControls() {
    return (this.questionForm.get('answers') as FormArray).controls;
  }

  onAddAnswer() {
    (this.questionForm.get('answers') as FormArray).push(new FormGroup({
      id: new FormControl(''),
      answer: new FormControl(''),
      questionId: new FormControl(''),
      correct: new FormControl(false)
    }));
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    //  Ovo ne znam sta je
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
     // id: "",  mozda ne mora id
      answer: '',
      correct: false
    };

    const dialogRef = this.dialog.open(AnswerAddDialog, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Data result: ${result}`);
      // console.log('Ovo je DATA: ' + result.id);
      if (result) {
        const answer = {
           id: null, // mozda ne mora id
          questionId: this.id,
          answer: dialogConfig.data.answer,
          correct: dialogConfig.data.correct
        };
        this.answersService.addAnswer(answer).subscribe(
          (a: Answer) => {
            // alert('You added an answer!');
            (this.questionForm.get('answers') as FormArray)
              .push(new FormGroup(
                {
                  answer: new FormControl(a.answer),
                  correct: new FormControl(a.correct),
                  questionId: new FormControl(answer.questionId),
                  id: new FormControl(a.id)}));
            // this.questionService.getQuestion(this.id).subscribe(
            //
            // );
          }
        );
        // console.log('Da li me vidis ' + dialogConfig.data.answer);
        // console.log('How about you ' + dialogConfig.data.correct);

      }
      // if (result) {
        //  NO NEGO POST NA SERVER api/questions/this.id/answers pa ce on sam fetch sa servera
        // (this.questionForm.get('answers') as FormArray).push(new FormGroup({
        //   id: new FormControl(''),
        //   answer: new FormControl(''),
        //   questionId: new FormControl(this.id),
        //   correct: new FormControl(false)
        // }));
      // }
    });
  }

  // DA LI TREBA OVDE
  // ngOnChanges(changes: SimpleChanges): void {
  //
  // }
  onDelete(questionId: number, id: number) {
    this.answersService.deleteAnswer(questionId, id).subscribe(
      (deletedAnswer: Answer) => {
        ((this.questionForm.get('answers') as FormArray).value).forEach((answer, index) => {
          if (answer.id === id) {
            (this.questionForm.get('answers') as FormArray).removeAt(index);
          }
        });
      }
    );
  }
}
