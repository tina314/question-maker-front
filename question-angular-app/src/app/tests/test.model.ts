import {Question} from '../questions/question.model';

export class Test {
  public id: number;
  public title: string;
  public questions: Question[];

  constructor(id: number, title: string, questions: Question[]) {
    this.id = id;
    this.title = title;
    this.questions = questions;
  }
}
