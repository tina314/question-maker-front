import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Test Maker App';

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.fetchQuestions();
  }

  onFetchQuestions() {
    this.fetchQuestions();
  }

  private fetchQuestions() {
    this.http.get('http://localhost:8080/api/questions').subscribe(
      questions => console.log(questions)
    );
  }
}
