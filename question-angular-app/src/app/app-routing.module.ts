import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {QuestionsComponent} from "./questions/questions.component";
import {QuestionEditComponent} from "./questions/question-edit/question-edit.component";
import {QuestionNewComponent} from "./questions/question-new/question-new.component";
import {TestsComponent} from "./tests/tests.component";
import {TestNewComponent} from "./tests/test-new/test-new.component";


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'questions',
    component: QuestionsComponent, pathMatch: 'full' },
  { path: 'questions/new', component: QuestionNewComponent },
  { path: 'questions/:id/edit', component: QuestionEditComponent },
  { path: 'tests', component: TestsComponent },
  { path: 'tests/new', component: TestNewComponent },
  { path: 'home', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
