import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {MatSliderModule} from '@angular/material/slider';
import { AppToolbarComponent } from './app-toolbar/app-toolbar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { HomeComponent } from './home/home.component';
import { QuestionsComponent } from './questions/questions.component';
import { AnswerComponent } from './questions/answer/answer.component';
import { QuestionEditComponent } from './questions/question-edit/question-edit.component';
import { QuestionListComponent } from './questions/question-list/question-list.component';
import { QuestionItemComponent } from './questions/question-list/question-item/question-item.component';
import {HttpClientModule} from "@angular/common/http";
import { QuestionNewComponent } from './questions/question-new/question-new.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {AnswerAddDialog} from "./questions/question-edit/answer-add-dialog";
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from "@angular/material/dialog";
import { TestsComponent } from './tests/tests.component';
import { TestListComponent } from './tests/test-list/test-list.component';
import { TestNewComponent } from './tests/test-new/test-new.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatGridListModule} from "@angular/material/grid-list";
import {MatPaginatorModule} from "@angular/material/paginator";

@NgModule({
  declarations: [
    AppComponent,
    AppToolbarComponent,
    HomeComponent,
    QuestionsComponent,
    AnswerComponent,
    QuestionEditComponent,
    QuestionListComponent,
    QuestionItemComponent,
    QuestionNewComponent,
    AnswerAddDialog,
    TestsComponent,
    TestListComponent,
    TestNewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NoopAnimationsModule,
    MatSliderModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatDialogModule,
    MatFormFieldModule,
    MatGridListModule,
    MatPaginatorModule
  ],
  entryComponents: [QuestionListComponent, AnswerAddDialog],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  bootstrap: [AppComponent, QuestionListComponent]
})
export class AppModule { }
